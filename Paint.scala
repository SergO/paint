package paint

/**
  * Created by SergO on 14.3.17.
  */
class Paint(initCanvas: List[List[Int]]) {

  private var canvas = initCanvas

  /*
  * Changes color of one pixel
  * */
  def paintPixel(coord: Coord, color: Int): Unit = {
    val canvas = for (rowNum <- this.canvas.indices.toList)
      yield for (colNum <- this.canvas(rowNum).indices.toList)
        yield if (coord.rowIdx == rowNum && coord.colIdx == colNum) color else this.canvas(rowNum)(colNum)

    this.canvas = canvas
  }

  /*
  * Returns color of a pixel
  * */
  def pixel(coord: Coord): Int = this.canvas(coord.rowIdx)(coord.colIdx)


  /*
  * Paints a solid one-colored area by another color
  * */
  def fill(x: Int, y: Int, color: Int): Unit = {
//    inverting coordinated to internal structure
    val coord = Coord(y, x)

    val origColor = this.canvas(coord.rowIdx)(coord.colIdx)

    /*
    * A recursive searching of neighbour same-colored pixels
    * */
    def filler(coord: Coord): Unit = {
      this.paintPixel(coord, color)

      if (coord.rowIdx > 0 && this.pixel(Coord(coord.rowIdx - 1, coord.colIdx)) == origColor)
        filler(Coord(coord.rowIdx - 1, coord.colIdx))

      if (coord.colIdx > 0 && this.pixel(Coord(coord.rowIdx, coord.colIdx - 1)) == origColor)
        filler(Coord(coord.rowIdx, coord.colIdx - 1))

      if (coord.rowIdx + 1 < canvas.length && this.pixel(Coord(coord.rowIdx + 1, coord.colIdx)) == origColor)
        filler(Coord(coord.rowIdx + 1, coord.colIdx))

      if (coord.colIdx + 1 < canvas(coord.rowIdx).length && this.pixel(Coord(coord.rowIdx, coord.colIdx + 1)) == origColor)
        filler(Coord(coord.rowIdx, coord.colIdx + 1))
    }

    if (origColor != color)
      filler(coord)
  }

  /*
  * Outputs canvas to the screen
  * */
  def show(): Unit = {
    for (row <- this.canvas) {
      for (pixel <- row) {
        print(pixel)
      }
      println()
    }
    println()
  }
}
