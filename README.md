# a very simple graphic editor #

This is an emulation of simple graphic editor with implemented a method `fill(x, y, color)`

### Requirements ###

* Scala (2.12)

### How to run application ###

```bash
$> cd path/to/application
$> scalac *.scala
$> scala paint.PaintApp
```
