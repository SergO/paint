package paint

/**
  * Created by SergO on 14.3.17.
  */
case class Coord(rowIdx: Int, colIdx: Int)

object Coord
