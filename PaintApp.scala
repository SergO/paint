package paint

/**
  * Created by SergO on 14.3.17.
  */

object PaintApp {
  def main(args: Array[String]): Unit = {
    val paint = new Paint(List(
      List(0, 4, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1),
      List(0, 4, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1),
      List(0, 4, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1),
      List(0, 4, 0, 0, 0, 2, 7, 7, 7, 7, 7, 7, 1),
      List(0, 4, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1),
      List(0, 4, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 1),
      List(0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 1),
      List(0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 1)
    ))

    println("init canvas")
    println("-------------")
    paint.show()

    println("fill(2, 1, 5)")
    println("-------------")
    paint.fill(2, 1, 5)
    paint.show()

    println("fill(0, 5, 6)")
    println("-------------")
    paint.fill(0, 5, 6)
    paint.show()

  }
}